module XingApi
  class Client
    include XingApi::ResponseHandler

    OAUTH_ATTRIBUTES = [:consumer_key, :consumer_secret, :oauth_token, :oauth_token_secret]
    attr_writer *OAUTH_ATTRIBUTES
    attr_accessor :request_token_hash

    class << self
      attr_accessor :default_options

      def configure(&block)
        instance = self.new
        yield instance
        self.default_options = instance.send(:to_hash)
      end
    end # class << self

    def initialize(options={})
      options = (self.class.default_options ||= {}).merge(options)
      OAUTH_ATTRIBUTES.each do |attribute|
        send "#{attribute}=", options[attribute]
      end
    end

    def request(http_verb, url, options={})
      use_rest_client = options.delete(:use_rest_client) || false

      if not use_rest_client
        full_url = url + hash_to_params(options)
        handle(access_token.request(http_verb, full_url))
      else
        RestClient.add_before_execution_proc do |req, params|
          access_token.sign! req
        end
        site = ENV['XING_API_SITE'] || 'https://api.xing.com'
        url = "#{site}#{url}"
        if http_verb == :post
          puts "posting a job to: #{url}"
          begin
            if options[:payload]
              response = RestClient.post url, options[:payload], :content_type => :json, :accept => :json
            else
              puts "<< DEBUG JSON OUTPUT >>"
              puts options.to_json
              response = RestClient.post url, options.to_json, :content_type => :json, :accept => :json
            end
            response
          rescue => e
            e
          end
        elsif http_verb == :get
          begin
            RestClient.get url, :content_type => :json, :accept => :json
          rescue => e
            e
          end
        elsif http_verb == :put
          begin
            if options[:payload]
              response = RestClient.put url, options[:payload], :content_type => :json, :accept => :json
            else
              response = RestClient.put url, options.to_json, :content_type => :json, :accept => :json
            end
            response
          rescue => e
            e
          end
        elsif http_verb == :delete
          begin
            RestClient.delete url, :content_type => :json, :accept => :json
          rescue => e
            e
          end
        else
          puts "no valid http method found: #{http_verb}"
        end
      end
    end

    def get_request_token(oauth_callback='oob')
      ensure_attributes_are_set! %w(consumer_key consumer_secret)

      request_token = request_token(oauth_callback)
      self.request_token_hash = {
        request_token: request_token.token,
        request_token_secret: request_token.secret,
        authorize_url: request_token.authorize_url
      }
    end

    def get_access_token(verifier, options={})
      ensure_attributes_are_set! %w(consumer_key consumer_secret)

      options = request_token_hash.merge(options) if request_token_hash
      request_token = options[:request_token] || raise('request_token missing')
      request_token_secret = options[:request_token_secret] || raise('request_token_secret missing')

      request_token = OAuth::RequestToken.new(consumer, request_token, request_token_secret)
      access_token = request_token.get_access_token(:oauth_verifier => verifier)
      self.oauth_token = access_token.token
      self.oauth_token_secret = access_token.secret
      {
        access_token: access_token.token,
        access_token_secret: access_token.secret
      }
    end

    OAUTH_ATTRIBUTES.each do |attribute|
      define_method(attribute) do
        instance_variable_get("@#{attribute}") || self.class.default_options[attribute]
      end
    end

    private

    def to_hash
      {
        :consumer_key => consumer_key,
        :consumer_secret => consumer_secret,
        :oauth_token => oauth_token,
        :oauth_token_secret => oauth_token_secret
      }
    end

    def request_token(oauth_callback)
      @request_token ||= consumer.get_request_token(:oauth_callback => oauth_callback)
    end

    @@counter = 0

    def consumer
      #TODO: find out why this counter is called so many times :)
      #puts "SITE set to #{xing_oauth_options[:site]}"
      @@counter = @@counter + 1
      OAuth::Consumer.new(consumer_key, consumer_secret, xing_oauth_options)
    end

    def access_token
      OAuth::AccessToken.new(consumer, oauth_token, oauth_token_secret)
    end

    def xing_oauth_options
      {
        :site               => ENV['XING_API_SITE'] || 'https://api.xing.com',
        :request_token_path => '/v1/request_token',
        :authorize_path     => '/v1/authorize',
        :access_token_path  => '/v1/access_token',
        :signature_method   => 'PLAINTEXT',
        :oauth_version      => '1.0',
        :scheme             => 'query_string'
      }
    end

    def hash_to_params(hash)
      return '' if hash.empty?
      '?' + hash.map {|k,v| "#{k}=#{CGI.escape(v.to_s)}"}.join('&')
    end

    def ensure_attributes_are_set!(attribute_names)
      Array(attribute_names).each do |attribute_name|
        raise "#{attribute_name} is missing" unless send(attribute_name)
      end
    end

  end
end

module XingApi
  class NetworkFeed < XingApi::Base
    require "cgi"

    def self.network_feed(user_id, options={})
      request(:get, "/v1/users/#{user_id}/network_feed", options)
    end

    def self.user_feed(user_id, options={})
      request(:get, "/v1/users/#{user_id}/feed", options)
    end

    def self.status_message(user_id, message, options={})
      request(:post, "/v1/users/#{user_id}/status_message", {
        :message => message
      }.merge(options))
    end

    def self.link(link, options={})
      uri = CGI.escape(link)
      request(:post, "/v1/users/me/share/link", {
        :uri => uri
      }.merge(options))
    end

    def self.like(activity_id, options={})
      request(:put, "/v1/activities/#{activity_id}/like", options)
    end

  end
end

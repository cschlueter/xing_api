module XingApi
  module Vendor

    class Project < XingApi::Base
      def self.create(options={})
        options[:use_rest_client] = true
        options[:project] = options
        request(:post, "/vendor/projects/projects", options)
      end

      def self.preview(options={})
        options[:use_rest_client] = true
        options[:api_preview] = true
        options[:project] = options
        request(:post, "/vendor/projects/projects", options)
      end

      def self.balance(options={})
        options[:use_rest_client] = true
        request(:get, "/vendor/projects/projects/balance", options)
      end
    end

    class JobPosting < XingApi::Base
      def self.create(options={})
        options[:use_rest_client] = true
        request(:post, "/vendor/jobs/postings", options)
      end

      def self.print_response(response)
        puts ">> --"
        puts ">> http.code #{response.code}"
        puts ">> http.body #{response.body}"
        puts ">> --"
      end
      
      def self.update(posting_id, options={})
        options[:use_rest_client] = true
        response = request(:put, "/vendor/jobs/postings/#{posting_id}", options)
        print_response response if options[:debug]
        response
      end

      def self.activate(posting_id, options={})
        options[:use_rest_client] = true
        request(:put, "/vendor/jobs/postings/#{posting_id}/activate", options)
      end

      def self.deactivate(posting_id, options={})
        options[:use_rest_client] = true
        request(:put, "/vendor/jobs/postings/#{posting_id}/deactivate", options)
      end

      def self.archive(posting_id, options={})
        options[:use_rest_client] = true
        request(:put, "/vendor/jobs/postings/#{posting_id}/archive", options)
      end

      def self.delete(posting_id, options={})
        options[:use_rest_client] = true
        request(:delete, "/vendor/jobs/postings/#{posting_id}", options)
      end

      def self.legacy_show(posting_id, options={})
        options[:use_rest_client] = true
        request(:get, "/xws/marketplace/job_postings/#{posting_id}", options)
      end

      def self.list(posting_ids='', page=0, options={})
        options[:use_rest_client] = true

        request(:get, '/vendor/jobs/postings', {
          :ids => posting_ids,
          :page => page
        }.merge(options))

      end

    end
  end
end

